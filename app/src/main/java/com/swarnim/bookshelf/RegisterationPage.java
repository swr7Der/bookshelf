package com.swarnim.bookshelf;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.swarnim.bookshelf.constants.constants;

import org.json.JSONException;
import org.json.JSONObject;

public class RegisterationPage extends AppCompatActivity {

    EditText firstName,lastName,email,password,confirmPass,college,batch,dept;
    Button register;
    TextView signIn;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registeration_page);

        firstName = (EditText) findViewById(R.id.firstName);
        lastName = (EditText) findViewById(R.id.lastName);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.pass);
        confirmPass = (EditText) findViewById(R.id.confirmPass);
        college = (EditText) findViewById(R.id.college);
        batch = (EditText) findViewById(R.id.batch);
        dept = (EditText) findViewById(R.id.department);
        register = (Button) findViewById(R.id.register);
        signIn = (TextView) findViewById(R.id.loginNow);

        sharedPreferences = getSharedPreferences(constants.storage,MODE_PRIVATE);

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerNow(String.valueOf(firstName.getText()),String.valueOf(lastName.getText()),
                        String.valueOf(email.getText()),String.valueOf(password.getText()),
                        String.valueOf(confirmPass.getText()),String.valueOf(college.getText()),
                        String.valueOf(batch.getText()),String.valueOf(dept.getText()));
            }
        });

        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(RegisterationPage.this,MainActivity.class);
                startActivity(i);
            }
        });
    }

    public void registerNow(String firstName, String lastName, final String email, final String password,
                            String confirmPass, String college, String batch, String dept){

        if (firstName.length()>0 && lastName.length()>0 && email.length()>0 && password.length()>0 &&
                confirmPass.length()>0 && college.length()>0 && batch.length()>0 && dept.length()>0){

            if (password.equals(confirmPass)){

                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("firstName",firstName);
                    jsonObject.put("lastName",lastName);
                    jsonObject.put("email",email);
                    jsonObject.put("college",college);
                    jsonObject.put("department",dept);
                    jsonObject.put("batch",batch);
                    jsonObject.put("password",password);
                    jsonObject.put("confirm",confirmPass);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                        constants.base_url + "auth/register", jsonObject,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    String error = response.getJSONObject("meta").getString("error");
                                    String message = response.getJSONObject("data").getString("msg");
                                    if (error.length() == 0){
                                        Toast.makeText(getApplicationContext(),message,
                                                Toast.LENGTH_SHORT).show();

                                        int status = response.getJSONObject("meta").getInt("status");
                                        if (status == 201){
                                            editor = sharedPreferences.edit();
                                            Intent i = new Intent(RegisterationPage.this,BooksList.class);
                                            i.putExtra("type","REGISTER");
                                            editor.putString("email",response.getJSONObject("data").getString("email"));
                                            editor.putString("password",password);
                                            editor.apply();
                                            i.putExtra("email",response.getJSONObject("data").getString("email"));
                                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                            startActivity(i);
                                        }

                                    } else {
                                        Toast.makeText(getApplicationContext(),error,
                                                Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                Log.i("Result", "1" + response);
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (String.valueOf(error.networkResponse) != null){
                            if (!String.valueOf(error.networkResponse.statusCode).equals("201")){
                                Toast.makeText(getApplicationContext(),"Something went wrong!",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                        //Log.i("Result_error","1"+error.getMessage()+String.valueOf(error)+String.valueOf(error.networkResponse.statusCode));
                    }
                });

                RequestQueue queue = Volley.newRequestQueue(this);
                queue.add(jsonObjectRequest);

            } else {
                Toast.makeText(this,"Password doesn't match",Toast.LENGTH_SHORT).show();
            }

        } else {
            Toast.makeText(this,"All fields are necessary",Toast.LENGTH_SHORT).show();
        }

    }
}
