package com.swarnim.bookshelf;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.swarnim.bookshelf.adapter.EditBooksListAdapter;
import com.swarnim.bookshelf.constants.constants;
import com.swarnim.bookshelf.helper.BookToAdd;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Books extends AppCompatActivity {

    RecyclerView recyclerView;
    EditBooksListAdapter adapter;
    ArrayList<BookToAdd> bookList;
    RecyclerView.LayoutManager layoutManager;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    FloatingActionButton saveSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_books);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
        bookList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        saveSettings = (FloatingActionButton) findViewById(R.id.saveSettings);

        sharedPreferences = getSharedPreferences(constants.storage,MODE_PRIVATE);
        final String email = sharedPreferences.getString("email",null);

        Intent i = getIntent();
        String operation = null;
        if (i.hasExtra("operation")){
            operation = i.getStringExtra("operation");

            if (!operation.equals("DELETE")){
                showAllBooks(email,operation);
            } else {
                deleteWishList(email);
            }

        }

        sharedPreferences = getSharedPreferences(constants.storage,MODE_PRIVATE);

        final String finalOperation = operation;
        saveSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save_settings(email, finalOperation);
            }
        });
    }

    public void showAllBooks(final String email, final String operation){
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                constants.base_url + "books/listBooks", null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String error = response.getJSONObject("meta").getString("error");
                            if (error.length() == 0){

                                int status = response.getJSONObject("meta").getInt("status");
                                if (status == 200){
                                    Log.i("Result",String.valueOf(response));

                                    if (operation.equals("EDIT")) {
                                        showWishList(email,operation,response.getJSONArray("data"));
                                        getSupportActionBar().setTitle("Edit Wishlist");
                                    } else {
                                        showWishList(email,operation,response.getJSONArray("data"));
                                        getSupportActionBar().setTitle("Create Wishlist");
                                    }
                                }

                            } else {
                                Toast.makeText(getApplicationContext(),error,
                                        Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.i("Result", "1" + response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (String.valueOf(error.networkResponse) != null){
                    if (!String.valueOf(error.networkResponse.statusCode).equals("200")){
                        Toast.makeText(getApplicationContext(),"Something went wrong.Please restart the app!",
                                Toast.LENGTH_SHORT).show();
                        Log.i("Result_error1", error.getMessage());
                    }
                }
            }
        });

        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(jsonObjectRequest);
    }

    public void showWishList(final String email, final String operation, final JSONArray allBooks){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email",email);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                constants.base_url + "wishlist/getWishlist", jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String error = response.getJSONObject("meta").getString("error");
                            if (error.length() == 0){

                                int status = response.getJSONObject("meta").getInt("status");
                                if (status == 200){
                                    Log.i("Result",String.valueOf(response));

                                    JSONArray books = response.getJSONObject("data").getJSONArray("books");

                                    showBooks(email,operation,allBooks,books);
                                }

                            } else {
                                Toast.makeText(getApplicationContext(),error,
                                        Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.i("Result", "1" + response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (String.valueOf(error.networkResponse) != null){
                    /*Toast.makeText(getApplicationContext(),"Something went wrong: "+error.getMessage(),
                            Toast.LENGTH_SHORT).show();
                    //Log.i("Result_error2", error.getMessage());*/
                    if (String.valueOf(error.networkResponse.statusCode).equals("400")){
                        JSONArray books = new JSONArray();
                        showBooks(email,operation,allBooks,books);
                        //Toast.makeText(getApplicationContext(),"No wishlist found!",
                                //Toast.LENGTH_SHORT).show();
                    }  else if (!String.valueOf(error.networkResponse.statusCode).equals("200")){
                        Toast.makeText(getApplicationContext(),"Something went wrong.Please restart the app!",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(jsonObjectRequest);
    }

    public void showBooks(String email,String operation,JSONArray allBooks,JSONArray wishList){
        for (int i=0; i<allBooks.length(); i++){
            JSONObject jsonObject = null;
            try {
                jsonObject = allBooks.getJSONObject(i);
                String bookname = jsonObject.getString("name");
                String authors = jsonObject.getString("authors");
                String id = jsonObject.getString("id");
                String availability = jsonObject.getString("availibility");

                BookToAdd singlebook = null;

                StringBuilder stringToSave = new StringBuilder();

                if (wishList.length()>0){
                    for (int j=0; j<wishList.length(); j++){
                        if (id.equals(wishList.getString(j))){
                            singlebook = new BookToAdd(id,bookname,authors,availability,true);
                            break;
                        }
                        if (!id.equals(wishList.getString(j)) && j==wishList.length()-1){
                            singlebook = new BookToAdd(id,bookname,authors,availability,false);
                        }
                        stringToSave.append(" "+wishList.getString(j)+",");
                    }
                } else {
                    singlebook = new BookToAdd(id,bookname,authors,availability,false);
                }

                editor = sharedPreferences.edit();
                editor.putString("wishlist",String.valueOf(stringToSave));
                editor.apply();

                bookList.add(singlebook);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        adapter = new EditBooksListAdapter(this,bookList);
        recyclerView.setAdapter(adapter);
    }

    public void save_settings(String email, final String type){
        String toSave = sharedPreferences.getString("wishlist",null);
        String[] split = toSave.split(",");

        JSONArray ids = new JSONArray();

        for (int i=0; i<split.length; i++){
            String a = split[i].replace(" ","");
            Log.i("Result",a);
            try {
                ids.put(i,a);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email",email);
            jsonObject.put("books",ids);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String urlend = null;
        if (type.equals("CREATE")){
            urlend = "wishlist/createWishlist";
        } else if (type.equals("EDIT")){
            urlend = "wishlist/editWishlist";
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                constants.base_url + urlend, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String error = response.getJSONObject("meta").getString("error");
                            if (error.length() == 0){

                                int status = response.getJSONObject("meta").getInt("status");
                                if (status == 201){
                                    Log.i("Result",String.valueOf(response));

                                    if (type.equals("CREATE")){
                                        Toast.makeText(getApplicationContext(),"Wishlist Created Successfully",
                                                Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(getApplicationContext(),"Wishlist updated Successfully",
                                                Toast.LENGTH_SHORT).show();
                                    }

                                    Intent i = new Intent(Books.this,BooksList.class);
                                    i.putExtra("type","LOGIN");
                                    i.putExtra("email",response.getJSONObject("data").getString("email"));
                                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                    startActivity(i);
                                }

                            } else {
                                Toast.makeText(getApplicationContext(),error,
                                        Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.i("Result", "1" + response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (String.valueOf(error.networkResponse) != null){
                    if (!String.valueOf(error.networkResponse.statusCode).equals("200") && type.equals("CREATE")){
                        Toast.makeText(getApplicationContext(),"Wishlist was already created earlier. Go to Edit Wishlist section to edit wishlist!",
                                Toast.LENGTH_LONG).show();
                    }
                     else if (!String.valueOf(error.networkResponse.statusCode).equals("200")){
                        Toast.makeText(getApplicationContext(),"Something went wrong.Please restart the app!",
                                Toast.LENGTH_SHORT).show();
                    }
                }
                Log.i("Result", "2" + error);
            }
        });

        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(jsonObjectRequest);
    }

    public void deleteWishList(final String email){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email",email);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                constants.base_url + "wishlist/deleteWishlist", jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String error = response.getJSONObject("meta").getString("error");
                            if (error.length() == 0){

                                int status = response.getJSONObject("meta").getInt("status");
                                if (status == 200){
                                    Toast.makeText(getApplicationContext(),"Wishlist Deleted Successfully",
                                            Toast.LENGTH_SHORT).show();

                                    Log.i("Result",String.valueOf(response));
                                    Intent i = new Intent(Books.this,BooksList.class);
                                    i.putExtra("type","REGISTER");
                                    i.putExtra("email",email);
                                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                    startActivity(i);
                                }

                            } else {
                                Toast.makeText(getApplicationContext(),error,
                                        Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.i("Result", "1" + response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (String.valueOf(error.networkResponse) != null){
                    if (!String.valueOf(error.networkResponse.statusCode).equals("200")){
                        Toast.makeText(getApplicationContext(),"Something went wrong.Please restart the app!",
                                Toast.LENGTH_SHORT).show();
                    }
                }
                Log.i("Result", "2" + error);
            }
        });

        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(jsonObjectRequest);
    }
}
