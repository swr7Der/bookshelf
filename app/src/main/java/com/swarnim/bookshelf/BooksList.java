package com.swarnim.bookshelf;

import android.Manifest;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.swarnim.bookshelf.adapter.BookListAdapter;
import com.swarnim.bookshelf.constants.constants;
import com.swarnim.bookshelf.helper.Singlebook;
import com.ufobeaconsdk.callback.OnConnectSuccessListener;
import com.ufobeaconsdk.callback.OnFailureListener;
import com.ufobeaconsdk.callback.OnScanSuccessListener;
import com.ufobeaconsdk.main.UFOBeaconManager;
import com.ufobeaconsdk.main.UFODevice;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.Permission;
import java.util.ArrayList;

public class BooksList extends AppCompatActivity {

    private Boolean exit;
    RecyclerView recyclerView;
    BookListAdapter adapter;
    ArrayList<Singlebook> bookList;
    RecyclerView.LayoutManager layoutManager;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    FloatingActionButton fab;

    ImageButton scanNow;

    ImageView logout;
    String finalType;

    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_books_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
        bookList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new BookListAdapter(this,bookList);
        recyclerView.setAdapter(adapter);
        scanNow = (ImageButton) findViewById(R.id.scanNow);

        sharedPreferences = getSharedPreferences(constants.storage,MODE_PRIVATE);
        logout = (ImageView) findViewById(R.id.logout);

        fab = (FloatingActionButton) findViewById(R.id.fab);

        Intent i = getIntent();

        String email = null;
        String type = null;

        if (i.hasExtra("type")){
            type = i.getStringExtra("type");
            email = i.getStringExtra("email");
            showAllBooks(email,type);
        }

        /*editor = sharedPreferences.edit();
        editor.putBoolean("connected",true);
        editor.apply();*/

        if (sharedPreferences.contains("connected")){
            if (sharedPreferences.getBoolean("connected",false)){
                recyclerView.setVisibility(View.VISIBLE);
                scanNow.setVisibility(View.GONE);
                scanNow.setClickable(false);
                fab.setVisibility(View.VISIBLE);
            } else {
                recyclerView.setVisibility(View.GONE);
                scanNow.setVisibility(View.VISIBLE);
                scanNow.setClickable(true);
                fab.setVisibility(View.GONE);
            }
        }

        finalType = type;
        scanNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                askForPermission(Manifest.permission.ACCESS_COARSE_LOCATION,PERMISSION_REQUEST_COARSE_LOCATION);
            }
        });

        exit = false;

        final String finalEmail = email;
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(BooksList.this);
                String[] items = {"Create Wishlist","Edit Wishlist", "Delete Wishlist"};
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0){
                            isWishList(finalEmail);
                        } else if (which == 1){
                            Intent i = new Intent(BooksList.this,Books.class);
                            i.putExtra("operation","EDIT");
                            startActivity(i);
                        } else {
                            Intent i = new Intent(BooksList.this,Books.class);
                            i.putExtra("operation","DELETE");
                            startActivity(i);
                        }
                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();

            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(BooksList.this);
                builder.setTitle("Logout?");
                builder.setMessage("Are you sure you want to logout?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        logoutNow(sharedPreferences.getString("email",null));
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (exit) {
            finish(); // finish activity
        } else {
            Toast.makeText(this, "Press Back again to Exit.",
                    Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);

        }
    }

    public void showAllBooks(final String email, final String type){
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                constants.base_url + "books/listBooks", null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String error = response.getJSONObject("meta").getString("error");
                            if (error.length() == 0){

                                int status = response.getJSONObject("meta").getInt("status");
                                if (status == 200){
                                    Log.i("Result",String.valueOf(response));

                                    if (scanNow.getVisibility() == View.GONE){
                                        if (type.equals("REGISTER")) {
                                            getSupportActionBar().setTitle("All Books");
                                        } else {
                                            getSupportActionBar().setTitle("My Wishlist");
                                        }
                                    } else {
                                        getSupportActionBar().setTitle("Scan now to access Library");
                                    }

                                    if (type.equals("REGISTER")) {
                                        showBooks(response.getJSONArray("data"));
                                    } else {
                                        showWishList(email,response.getJSONArray("data"));
                                    }
                                }

                            } else {
                                Toast.makeText(getApplicationContext(),error,
                                        Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.i("Result", "1" + response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (String.valueOf(error.networkResponse) != null){
                    if (!String.valueOf(error.networkResponse.statusCode).equals("200")){
                        Toast.makeText(getApplicationContext(),"Something went wrong.Please restart the app!",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(jsonObjectRequest);
    }

    public void showBooks(JSONArray booksList){
        for (int i=0; i<booksList.length(); i++){
            JSONObject jsonObject = null;
            try {
                jsonObject = booksList.getJSONObject(i);
                String bookname = jsonObject.getString("name");
                String authors = jsonObject.getString("authors");
                String id = jsonObject.getString("id");
                String availability = jsonObject.getString("availibility");

                Singlebook singlebook = new Singlebook(id,bookname,authors,availability);
                bookList.add(singlebook);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        adapter.notifyDataSetChanged();
    }

    public void showWishList(String email, final JSONArray allBooks){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email",email);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                constants.base_url + "wishlist/getWishlist", jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String error = response.getJSONObject("meta").getString("error");
                            if (error.length() == 0){

                                int status = response.getJSONObject("meta").getInt("status");
                                if (status == 200){
                                    Log.i("Result",String.valueOf(response));

                                    JSONArray booksToShow = new JSONArray();

                                    JSONArray books = response.getJSONObject("data").getJSONArray("books");
                                    for (int i=0; i<books.length(); i++){
                                        int a = Integer.parseInt(books.getString(i));
                                        booksToShow.put(allBooks.get(a-1));
                                    }

                                    showBooks(booksToShow);

                                    if (booksToShow.length() <= 0){
                                        Toast.makeText(getApplicationContext(),"No wishlist available!",Toast.LENGTH_SHORT).show();
                                    }
                                }

                            } else {
                                Toast.makeText(getApplicationContext(),error,
                                        Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.i("Result", "1" + response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (String.valueOf(error.networkResponse) != null){
                    /*if (String.valueOf(error.networkResponse.statusCode).equals("400")){
                        Toast.makeText(getApplicationContext(),"No wishlist found!",
                                Toast.LENGTH_SHORT).show();
                    }
                    else if (!String.valueOf(error.networkResponse.statusCode).equals("200")){
                        Toast.makeText(getApplicationContext(),"Something went wrong.Please restart the app!",
                                Toast.LENGTH_SHORT).show();
                    }*/
                }
            }
        });

        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(jsonObjectRequest);
    }

    public void logoutNow(String email){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email",email);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                constants.base_url + "auth/logout", jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String error = response.getJSONObject("meta").getString("error");
                            if (error.length() == 0){

                                int status = response.getJSONObject("meta").getInt("status");
                                if (status == 200){
                                    Log.i("Result",String.valueOf(response));

                                    Intent i = new Intent(BooksList.this,MainActivity.class);
                                    editor = sharedPreferences.edit();
                                    editor.remove("email");
                                    editor.remove("password");
                                    editor.apply();
                                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                    startActivity(i);
                                }

                            } else {
                                Toast.makeText(getApplicationContext(),error,
                                        Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.i("Result", "1" + response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (String.valueOf(error.networkResponse) != null){
                    /*if (String.valueOf(error.networkResponse.statusCode).equals("400")){
                        Toast.makeText(getApplicationContext(),"No wishlist found!",
                                Toast.LENGTH_SHORT).show();
                    }
                    else if (!String.valueOf(error.networkResponse.statusCode).equals("200")){
                        Toast.makeText(getApplicationContext(),"Something went wrong.Please restart the app!",
                                Toast.LENGTH_SHORT).show();
                    }*/
                }
            }
        });

        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(jsonObjectRequest);
    }

    public void getWishListResp(final VolleyCallback callback, String email){

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email",email);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                constants.base_url + "wishlist/getWishlist", jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String error = response.getJSONObject("meta").getString("error");
                            if (error.length() == 0){

                                int status = response.getJSONObject("meta").getInt("status");
                                if (status == 200){
                                    Log.i("Result",String.valueOf(response));

                                    JSONArray books = response.getJSONObject("data").getJSONArray("books");

                                    if (books.length() <= 0){
                                        callback.onSuccess(false);
                                    } else {
                                        callback.onSuccess(true);
                                    }
                                }

                            } else {
                                Toast.makeText(getApplicationContext(),error,
                                        Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.i("Result", "1" + response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (String.valueOf(error.networkResponse) != null){
                    callback.onSuccess(false);
                }
            }
        });

        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(jsonObjectRequest);
    }

    public interface VolleyCallback{
        void onSuccess(boolean result);
    }

    public void isWishList(String email){

        getWishListResp(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result) {
                if (result){
                    Toast.makeText(BooksList.this,"You have already prepared your wishlist earlier.Click Edit Wishlist to edit your wishlist.",Toast.LENGTH_LONG).show();
                } else {
                    Intent i = new Intent(BooksList.this,Books.class);
                    i.putExtra("operation","CREATE");
                    startActivity(i);
                }
            }
        },email);
    }

    public void checkDevice(){
        final ProgressDialog progress = ProgressDialog.show(BooksList.this,"Scanning Devices","Please wait...");

        final UFOBeaconManager ufoBeaconManager = new UFOBeaconManager(getApplicationContext());
        ufoBeaconManager.startScan(new OnScanSuccessListener() {
            @Override
            public void onSuccess(UFODevice ufoDevice) {
                Log.i("Result1","true");
                ufoDevice.connect(new OnConnectSuccessListener() {
                    @Override
                    public void onSuccess(UFODevice ufoDevice) {

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                editor = sharedPreferences.edit();
                                editor.putBoolean("connected",true);
                                editor.apply();
                                progress.hide();
                                Toast.makeText(BooksList.this,"Device Connected Successfully",Toast.LENGTH_SHORT).show();
                                if (finalType.equals("REGISTER")) {
                                    getSupportActionBar().setTitle("All Books");
                                } else {
                                    getSupportActionBar().setTitle("My Wishlist");
                                }
                            }
                        });

                        BluetoothDevice bluetoothDevice = ufoDevice.getBtdevice();

                        //Success of having MAC Address of Beacon
                        Log.i("Result_UUID", "yeppppp " + String.valueOf(bluetoothDevice.getAddress()));

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                recyclerView.setVisibility(View.VISIBLE);
                                scanNow.setVisibility(View.GONE);
                                scanNow.setClickable(false);
                                fab.setVisibility(View.VISIBLE);
                            }
                        });

                    }
                }, new OnFailureListener() {
                    @Override
                    public void onFailure(int i, final String s) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progress.hide();
                                Toast.makeText(BooksList.this,s,Toast.LENGTH_LONG).show();                                    }
                        });

                    }
                });
            }
        }, new OnFailureListener() {
            @Override
            public void onFailure(int i, final String s) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progress.hide();
                        Toast.makeText(BooksList.this,"Some error has occurred: "+s,Toast.LENGTH_LONG).show();                                    }
                });

                Log.i("Result4",s);
            }
        });
    }

    private void askForPermission(String permission,Integer requestCode){
        if (ContextCompat.checkSelfPermission(BooksList.this,permission) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(BooksList.this,new String[]{permission},requestCode);
        } else {
            checkDevice();
        }
    }

    @Override
    public void onRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (ActivityCompat.checkSelfPermission(BooksList.this,permissions[0]) == PackageManager.PERMISSION_GRANTED){
            checkDevice();
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(BooksList.this);
            builder.setTitle("Need Permission");
            builder.setMessage("Since location access has not been granted, this app will not be able to discover beacons when in the background.");
            builder.setPositiveButton("Grant Permission", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    ActivityCompat.requestPermissions(BooksList.this,new String[]{permissions[0]},requestCode);
                }
            });
            builder.setNegativeButton("Deny Permission", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    Toast.makeText(BooksList.this,"This app won't work without access of location",Toast.LENGTH_SHORT).show();
                }
            });
            builder.setCancelable(false);

            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }
    }
}