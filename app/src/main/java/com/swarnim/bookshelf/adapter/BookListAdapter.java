package com.swarnim.bookshelf.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.swarnim.bookshelf.R;
import com.swarnim.bookshelf.helper.Singlebook;

import java.util.ArrayList;

/**
 * Created by swarnim on 30/5/17.
 */

public class BookListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context mContext;
    ArrayList<Singlebook> bookList;

    public BookListAdapter(Context mContext, ArrayList<Singlebook> bookList) {
        this.mContext = mContext;
        this.bookList = bookList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.z_single_book,parent,false);
        return new BookHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Singlebook singlebook = bookList.get(position);

        BookHolder bookHolder = (BookHolder) holder;
        bookHolder.bookName.setText(singlebook.getBookname());
        bookHolder.authorName.setText("Author(s): "+singlebook.getAuthorName());
        bookHolder.bookName.setTextColor(mContext.getResources().getColor(R.color.bookTakenText));
        bookHolder.authorName.setTextColor(mContext.getResources().getColor(R.color.bookTakenText));

        if (singlebook.getAvailibility().equals("1")){
            bookHolder.cardView.setBackgroundColor(mContext.getResources().getColor(R.color.bookAvail));
        } else {
            bookHolder.cardView.setBackgroundColor(mContext.getResources().getColor(R.color.bookNotAvail));
        }
    }

    @Override
    public int getItemCount() {
        return bookList.size();
    }

    public class BookHolder extends RecyclerView.ViewHolder{

        TextView bookName,authorName;
        CardView cardView;

        public BookHolder(View itemView) {
            super(itemView);

            bookName = (TextView) itemView.findViewById(R.id.bookName);
            authorName = (TextView) itemView.findViewById(R.id.authorName);
            cardView = (CardView) itemView.findViewById(R.id.cardView);
        }
    }
}