package com.swarnim.bookshelf.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.swarnim.bookshelf.R;
import com.swarnim.bookshelf.constants.constants;
import com.swarnim.bookshelf.helper.BookToAdd;
import com.swarnim.bookshelf.helper.Singlebook;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by swarnim on 30/5/17.
 */

public class EditBooksListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context mContext;
    ArrayList<BookToAdd> bookList;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    public EditBooksListAdapter(Context mContext, ArrayList<BookToAdd> bookList) {
        this.mContext = mContext;
        this.bookList = bookList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.z_single_book,parent,false);
        return new BookHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final BookToAdd singlebook = bookList.get(position);

        final BookHolder bookHolder = (BookHolder) holder;
        bookHolder.bookName.setText(singlebook.getBookname());
        bookHolder.authorName.setText("Author(s): "+singlebook.getAuthorName());

        sharedPreferences = mContext.getSharedPreferences(constants.storage,Context.MODE_PRIVATE);

        if (singlebook.getAdded()){
            bookHolder.cardView.setBackgroundColor(mContext.getResources().getColor(R.color.bookTaken));
            bookHolder.bookName.setTextColor(mContext.getResources().getColor(R.color.bookTakenText));
            bookHolder.authorName.setTextColor(mContext.getResources().getColor(R.color.bookTakenText));
        } else {
            bookHolder.cardView.setBackgroundColor(mContext.getResources().getColor(R.color.bookTakenText));
            bookHolder.bookName.setTextColor(mContext.getResources().getColor(R.color.bookNotTakeBackground));
            bookHolder.authorName.setTextColor(mContext.getResources().getColor(R.color.bookNotTakeBackground));
        }

        bookHolder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ColorDrawable colorDrawable = (ColorDrawable) bookHolder.cardView.getBackground();
                int color = colorDrawable.getColor();

                String savedList = sharedPreferences.getString("wishlist",null);
                String replacedString = null;

                if (color == mContext.getResources().getColor(R.color.bookTaken)){
                    if (savedList.contains(" "+singlebook.getId()+",")){
                        replacedString = savedList.replace(" "+singlebook.getId()+",","");
                    }
                    singlebook.setAdded(false);
                    bookHolder.cardView.setBackgroundColor(mContext.getResources().getColor(R.color.bookTakenText));
                    bookHolder.bookName.setTextColor(mContext.getResources().getColor(R.color.bookNotTakeBackground));
                    bookHolder.authorName.setTextColor(mContext.getResources().getColor(R.color.bookNotTakeBackground));
                } else {
                    if (!savedList.contains(" "+singlebook.getId()+",")){
                        replacedString = savedList+" "+singlebook.getId()+",";
                    }
                    singlebook.setAdded(true);
                    bookHolder.cardView.setBackgroundColor(mContext.getResources().getColor(R.color.bookTaken));
                    bookHolder.bookName.setTextColor(mContext.getResources().getColor(R.color.bookTakenText));
                    bookHolder.authorName.setTextColor(mContext.getResources().getColor(R.color.bookTakenText));
                }

                editor = sharedPreferences.edit();
                editor.putString("wishlist",replacedString);
                editor.apply();

                Log.i("Result","1"+sharedPreferences.getString("wishlist",null));
            }
        });

    }

    @Override
    public int getItemCount() {
        return bookList.size();
    }

    public class BookHolder extends RecyclerView.ViewHolder{

        TextView bookName,authorName;
        CardView cardView;
        LinearLayout layout;

        public BookHolder(View itemView) {
            super(itemView);

            bookName = (TextView) itemView.findViewById(R.id.bookName);
            authorName = (TextView) itemView.findViewById(R.id.authorName);
            cardView = (CardView) itemView.findViewById(R.id.cardView);
            layout = (LinearLayout) itemView.findViewById(R.id.layout);

        }
    }
}