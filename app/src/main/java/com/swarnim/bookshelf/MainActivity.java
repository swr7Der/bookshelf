package com.swarnim.bookshelf;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattDescriptor;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.ParcelUuid;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.swarnim.bookshelf.constants.constants;
import com.ufobeaconsdk.callback.OnConnectSuccessListener;
import com.ufobeaconsdk.callback.OnFailureListener;
import com.ufobeaconsdk.callback.OnReadSuccessListener;
import com.ufobeaconsdk.callback.OnScanSuccessListener;
import com.ufobeaconsdk.main.UFOBeaconManager;
import com.ufobeaconsdk.main.UFODevice;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    EditText signInEmail, signInPass;
    Button signIn;
    TextView registerPage;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        signInEmail = (EditText) findViewById(R.id.signinEmail);
        signInPass = (EditText) findViewById(R.id.signinPass);
        signIn = (Button) findViewById(R.id.signIn);
        registerPage = (TextView) findViewById(R.id.registerPage);

        progressDialog = new ProgressDialog(this,ProgressDialog.STYLE_SPINNER);

        sharedPreferences = getSharedPreferences(constants.storage,MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putBoolean("connected",false);
        editor.apply();

        if (sharedPreferences.contains("email") && sharedPreferences.contains("password")){
            progressDialog.setTitle("Signing in...");
            progressDialog.setMessage("Please wait...");
            progressDialog.show();

            dosignIn(sharedPreferences.getString("email",null),sharedPreferences.getString("password",null));
        }

        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (signInEmail.getText().length() > 0 && signInPass.getText().length() > 0){
                    dosignIn(String.valueOf(signInEmail.getText()),String.valueOf(signInPass.getText()));
                } else {
                    Toast.makeText(getApplicationContext(),"Please enter both email and password",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

        registerPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this,RegisterationPage.class);
                startActivity(i);
            }
        });
    }

    public void dosignIn(final String email, final String password){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email",email);
            jsonObject.put("password",password);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                constants.base_url + "auth/login", jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String error = response.getJSONObject("meta").getString("error");
                            String message = response.getJSONObject("data").getString("msg");
                            if (error.length() == 0){
                                Toast.makeText(getApplicationContext(),message,
                                        Toast.LENGTH_SHORT).show();

                                int status = response.getJSONObject("meta").getInt("status");
                                if (status == 201){

                                    if (progressDialog.isShowing()){
                                        progressDialog.hide();
                                    }

                                    editor = sharedPreferences.edit();
                                    Intent i = new Intent(MainActivity.this,BooksList.class);
                                    editor.putString("email",response.getJSONObject("data").getString("email"));
                                    editor.putString("password",password);
                                    editor.apply();
                                    i.putExtra("type","LOGIN");
                                    i.putExtra("email",response.getJSONObject("data").getString("email"));
                                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                    startActivity(i);
                                }

                            } else {
                                Toast.makeText(getApplicationContext(),error,
                                        Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.i("Result", "1" + response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (String.valueOf(error.networkResponse) != null){
                    if (!String.valueOf(error.networkResponse.statusCode).equals("201")){
                        Toast.makeText(getApplicationContext(),"Either email or password is incorrect!",
                                Toast.LENGTH_SHORT).show();
                    }
                }
                //Log.i("Result_error","1"+error.getMessage()+String.valueOf(error)+String.valueOf(error.networkResponse.statusCode));
            }
        });

        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(jsonObjectRequest);
    }
}
