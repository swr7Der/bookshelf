package com.swarnim.bookshelf.helper;

/**
 * Created by swarnim on 30/5/17.
 */

public class BookToAdd {

    String id,bookname,authorName,availibility;
    Boolean added;

    public BookToAdd(String id, String bookname, String authorName, String availibility, Boolean added) {
        this.id = id;
        this.bookname = bookname;
        this.authorName = authorName;
        this.availibility = availibility;
        this.added = added;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBookname() {
        return bookname;
    }

    public void setBookname(String bookname) {
        this.bookname = bookname;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAvailibility() {
        return availibility;
    }

    public void setAvailibility(String availibility) {
        this.availibility = availibility;
    }

    public Boolean getAdded() {
        return added;
    }

    public void setAdded(Boolean added) {
        this.added = added;
    }
}
