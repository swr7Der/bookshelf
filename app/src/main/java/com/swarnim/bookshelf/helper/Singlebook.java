package com.swarnim.bookshelf.helper;

/**
 * Created by swarnim on 30/5/17.
 */

public class Singlebook {

    String id,bookname,authorName,availibility;

    public Singlebook(String id, String bookname, String authorName, String availibility) {
        this.id = id;
        this.bookname = bookname;
        this.authorName = authorName;
        this.availibility = availibility;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBookname() {
        return bookname;
    }

    public void setBookname(String bookname) {
        this.bookname = bookname;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAvailibility() {
        return availibility;
    }

    public void setAvailibility(String availibility) {
        this.availibility = availibility;
    }
}
